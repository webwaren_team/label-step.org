<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Webwaren.' . $_EXTKEY,
	'Pi1',
	'Step Teppich Gallery'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Webwaren.' . $_EXTKEY,
	'Pi2',
	'Step Google Map'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Webwaren.' . $_EXTKEY,
	'Pi3',
	'Step Haendlerverzeichnis'
);

//$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Storage\\Typo3DbQueryParser'] = array(
//    'className' => 'Webwaren\\WwStep\\Utility\\Typo3DbQueryParser',
//);

$pluginSignature = str_replace('_', '', $_EXTKEY) . '_pi1';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	$pluginSignature,
	'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_pi1.xml'
);


$pluginSignature = str_replace('_', '', $_EXTKEY) . '_pi2';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	$pluginSignature,
	'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_pi2.xml'
);


$pluginSignature = str_replace('_', '', $_EXTKEY) . '_pi3';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_pi3.xml'
);

if (TYPO3_MODE === 'BE') {
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Webwaren.' . $_EXTKEY,
		'help',
		'cshmanual',
		'top',
		array(
			'Help' => 'index',
		),
		array(
			'access' => 'user,group',
			'labels' => 'LLL:EXT:'.$_EXTKEY.'/Resources/Private/Language/locallang_mod.xml',
		)
	);

}



\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Step');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_collection', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_collection.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_collection');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_country', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_country.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_country');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_carpet', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_carpet.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_carpet');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_art', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_art.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_art');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_material', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_material.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_material');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_group', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_group.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_group');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_wwstep_domain_model_color', 'EXT:ww_step/Resources/Private/Language/locallang_csh_tx_wwstep_domain_model_color.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_wwstep_domain_model_color');


#Page-View
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_art'][0]['fList'] = 'uid,name,description';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_carpet'][0]['fList'] = 'uid,title, art, material, color';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_collection'][0]['fList'] = 'uid,name, groups,brands';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_color'][0]['fList'] = 'uid,name,description';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_country'][0]['fList'] = 'uid,name,description';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_group'][0]['fList'] = 'uid,name';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['cms']['db_layout']['addTables']['tx_wwstep_domain_model_material'][0]['fList'] = 'uid,name,description';

