<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_collection',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,description,retail,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ww_step') . 'Resources/Public/Icons/tx_wwstep_domain_model_collection.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, description, groups,brands',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, name, description, groups,brands, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_wwstep_domain_model_collection',
				'foreign_table_where' => 'AND tx_wwstep_domain_model_collection.pid=###CURRENT_PID### AND tx_wwstep_domain_model_collection.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_collection.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_collection.description',
			'config' => array(
				'type' => 'text',
				'rows' => 10,
				'eval' => 'trim'
			),
			'defaultExtras' => 'richtext[]',
		),
		'groups' => array(
			'exclude' => 1,
			'label' => 'Händlergruppe',
            'l10n_mode' => '',
            'l10n_display' => 'defaultAsReadonly',
			'config' => array(
                'type' => 'select',
                'enableMultiSelectFilterTextfield' => TRUE,
                'renderType' => 'selectMultipleSideBySide',
                'internal_type' => 'db',
                'allowed' => 'tx_wwstep_domain_model_group',
                'foreign_table' => 'tx_wwstep_domain_model_group',
                'MM' => 'tx_wwstep_domain_model_group_mm',
                'size' => 5,
                'minitems' => 1,
                'maxitems' => 9999,
			),

		),
		'brands' => array(
			'exclude' => 1,
			'label' => 'Marke',
            'l10n_mode' => '',
            'l10n_display' => 'defaultAsReadonly',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'enableMultiSelectFilterTextfield' => TRUE,
				'foreign_table' => 'tx_wwstep_domain_model_group',
				'foreign_table_where' => 'AND tx_wwstep_domain_model_group.brands = 1',
				'foreign_sortby' => 'sorting',
				'maxitems' => 1,
			),

		),

	),
);
