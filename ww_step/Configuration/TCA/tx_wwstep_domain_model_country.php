<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_country',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'requestUpdate' => 'producer',
		'searchFields' => 'name,description,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ww_step') . 'Resources/Public/Icons/tx_wwstep_domain_model_country.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, description',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1,name, description,--palette--;1,1,--div--;Google Maps;googlemap,--palette--;1,1,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => 'producer,retailer,'),
		'googlemap' => array('showitem'=> 'configuration_map,--linebreak--,latitude, longitude,'),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_wwstep_domain_model_country',
				'foreign_table_where' => 'AND tx_wwstep_domain_model_country.pid=###CURRENT_PID### AND tx_wwstep_domain_model_country.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_country.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_country.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),

		'producer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_country.producer',
			'config' => array(
				'type' => 'check',
				'default' => '0',
			),
		),

		'retailer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_country.retailer',
			'config' => array(
				'type' => 'check',
				'default' => '0',
			),
		),

		'configuration_map' => array(
			'displayCond' => 'FIELD:producer:=:1',
			'exclude' => 0,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.configuration_map',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Webwaren\\WwStep\\Utility\\LocationUtility->render',
				'parameters' => array(
					'longitude' => 'longitude',
					'latitude' => 'latitude',
					'zoom' => '5',
				),
			),
		),
		'latitude' => array(
			'displayCond' => 'FIELD:producer:=:1',
			'exclude' => 0,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.latitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
				'default' => '47.029937'
			),
		),
		'longitude' => array(
			'displayCond' => 'FIELD:producer:=:1',
			'exclude' => 0,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.longitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim',
				'default' => '7.791563'
			),
		),

		'carpet' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'retail' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
