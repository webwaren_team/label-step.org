<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,images,art,material,color,collection,country,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ww_step') . 'Resources/Public/Icons/tx_wwstep_domain_model_carpet.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, images, art, material, color, collection, country',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, description,images, art, material, color, collection, country, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_wwstep_domain_model_carpet',
				'foreign_table_where' => 'AND tx_wwstep_domain_model_carpet.pid=###CURRENT_PID### AND tx_wwstep_domain_model_carpet.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_group.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
			),
			'defaultExtras' => 'richtext[]',
		),
		'images' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.images',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'images',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
					),
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						)
					),
					'maxitems' => 10
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'art' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.art',
			'config' => array(
				'type' => 'select',
				'enableMultiSelectFilterTextfield' => TRUE,
				'foreign_table' => 'tx_wwstep_domain_model_art',
				'foreign_sortby' => 'sorting',
                'foreign_table_where' => ' And sys_language_uid = 0',
				'MM' => 'tx_wwstep_carpet_art_mm',
				'maxitems' => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'material' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.material',
			'config' => array(
				'type' => 'select',
				'enableMultiSelectFilterTextfield' => TRUE,
				'foreign_table' => 'tx_wwstep_domain_model_material',
				'foreign_sortby' => 'sorting',
                'foreign_table_where' => ' And sys_language_uid = 0',
				'MM' => 'tx_wwstep_carpet_material_mm',
				'maxitems' => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'color' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.color',
			'config' => array(
				'type' => 'select',
				'enableMultiSelectFilterTextfield' => TRUE,
				'foreign_table' => 'tx_wwstep_domain_model_color',
				'foreign_sortby' => 'sorting',
                'foreign_table_where' => ' And sys_language_uid = 0',
				'MM' => 'tx_wwstep_carpet_color_mm',
				'maxitems' => 9999,
				'minitems' => 0,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'collection' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.collection',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'enableMultiSelectFilterTextfield' => TRUE,
				'foreign_table' => 'tx_wwstep_domain_model_collection',
                'foreign_table_where' => ' And sys_language_uid = 0',
				'maxitems' => 1,
			),
		),
		'country' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_carpet.country',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_wwstep_domain_model_country',
				'foreign_table_where' => 'AND producer=1 And sys_language_uid = 0',
				'foreign_sortby' => 'sorting',
				'minitems' => 0,
				'maxitems' => 1,
				'default' => 9,

			),

		),

	),
);
