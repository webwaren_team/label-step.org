<?php

if (!isset($GLOBALS['TCA']['pages']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['pages']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['pages']['ctrl']['dynamicConfigFile']);
	}
}

//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
//	'pages',
//	$GLOBALS['TCA']['pages']['ctrl']['type'],
//	'',
//	'after:' . $GLOBALS['TCA']['pages']['ctrl']['label']
//);

$tmp_ww_step_columns = array(

	'name' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.name',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'configuration_map' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.configuration_map',
		'config' => array(
			'type' => 'user',
			'userFunc' => 'Webwaren\\WwStep\\Utility\\LocationUtility->render',
			'parameters' => array(
				'longitude' => 'longitude',
				'latitude' => 'latitude',
				'zoom' => '12',
			),
		),
	),
	'latitude' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.latitude',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
			'default' => '46.947974'
		),
	),
	'longitude' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.longitude',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
			'default' => '7.447447'
		),
	),
	'retailtype' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.retailtype',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('Einzelhaendler', 0),
				array('Grosshaendler', 1),
			),
			'size' => 1,
			'maxitems' => 1,
			'eval' => ''
		),
	),
	'country' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.country',
		'config' => array(
			'type' => 'select',
			'renderType' => 'selectSingle',
			'foreign_table' => 'tx_wwstep_domain_model_country',
			'foreign_table_where' => 'AND retailer=1',
			'foreign_sortby' => 'sorting',
			'maxitems' => 1,
			'default' => 7,
		),

	),
	'groups' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.tx_wwstep_group',
		'config' => array(
			'type' => 'select',
			'renderType' => 'selectMultipleSideBySide',
			'enableMultiSelectFilterTextfield' => TRUE,
			'foreign_table' => 'tx_wwstep_domain_model_group',
			'foreign_sortby' => 'sorting',
			'maxitems' => 1,
		),

	),

	'images' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail.images',
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
			'images',
			array(
				'appearance' => array(
					'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
				),
				'foreign_types' => array(
					'0' => array(
						'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
						--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
						'showitem' => '
						--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
						--palette--;;filePalette'
					),
				),
				'maxitems' => 1
			),
			$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
		),
	),
	'info_window' => array(
		'exclude' => 1,
		'label' => 'Window Text',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 15,
			'eval' => 'trim',
			'wizards' => array(
				'RTE' => array(
					'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_rte.gif',
					'notNewRecords' => 1,
					'RTEonly' => 1,
					'module' => array(
						'name' => 'wizard_rte'
					),
					'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
					'type' => 'script'
				)
			)
		),
		'defaultExtras' => 'richtext[]',
	),
);

$tmp_ww_step_columns['collection'] = array(
	'config' => array(
		'type' => 'passthrough',
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tmp_ww_step_columns);

$GLOBALS['TCA']['pages']['types']['1']['showitem'] .= ',--div--;LLL:EXT:ww_step/Resources/Private/Language/locallang_db.xlf:tx_wwstep_domain_model_retail,';
$GLOBALS['TCA']['pages']['palettes']['googlemap']['showitem'] .= 'configuration_map,--linebreak--,latitude, longitude,';
$GLOBALS['TCA']['pages']['types']['1']['showitem'] .= '--palette--;Google Maps;googlemap,retailtype, groups,country,--div--;InfoWindow,images,info_window';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'',
	'EXT:/Resources/Private/Language/locallang_csh_.xlf'
);
