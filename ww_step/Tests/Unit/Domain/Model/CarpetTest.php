<?php

namespace Webwaren\WwStep\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Webwaren\WwStep\Domain\Model\Carpet.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Regna Pera <pera@webwaren.ch>
 */
class CarpetTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Webwaren\WwStep\Domain\Model\Carpet
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Webwaren\WwStep\Domain\Model\Carpet();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getImagesReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getImages()
		);
	}

	/**
	 * @test
	 */
	public function setImagesForFileReferenceSetsImages()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImages($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'images',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getArtReturnsInitialValueForArt()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getArt()
		);
	}

	/**
	 * @test
	 */
	public function setArtForObjectStorageContainingArtSetsArt()
	{
		$art = new \Webwaren\WwStep\Domain\Model\Art();
		$objectStorageHoldingExactlyOneArt = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneArt->attach($art);
		$this->subject->setArt($objectStorageHoldingExactlyOneArt);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneArt,
			'art',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addArtToObjectStorageHoldingArt()
	{
		$art = new \Webwaren\WwStep\Domain\Model\Art();
		$artObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$artObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($art));
		$this->inject($this->subject, 'art', $artObjectStorageMock);

		$this->subject->addArt($art);
	}

	/**
	 * @test
	 */
	public function removeArtFromObjectStorageHoldingArt()
	{
		$art = new \Webwaren\WwStep\Domain\Model\Art();
		$artObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$artObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($art));
		$this->inject($this->subject, 'art', $artObjectStorageMock);

		$this->subject->removeArt($art);

	}

	/**
	 * @test
	 */
	public function getMaterialReturnsInitialValueForMaterial()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getMaterial()
		);
	}

	/**
	 * @test
	 */
	public function setMaterialForObjectStorageContainingMaterialSetsMaterial()
	{
		$material = new \Webwaren\WwStep\Domain\Model\Material();
		$objectStorageHoldingExactlyOneMaterial = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneMaterial->attach($material);
		$this->subject->setMaterial($objectStorageHoldingExactlyOneMaterial);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneMaterial,
			'material',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addMaterialToObjectStorageHoldingMaterial()
	{
		$material = new \Webwaren\WwStep\Domain\Model\Material();
		$materialObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$materialObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($material));
		$this->inject($this->subject, 'material', $materialObjectStorageMock);

		$this->subject->addMaterial($material);
	}

	/**
	 * @test
	 */
	public function removeMaterialFromObjectStorageHoldingMaterial()
	{
		$material = new \Webwaren\WwStep\Domain\Model\Material();
		$materialObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$materialObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($material));
		$this->inject($this->subject, 'material', $materialObjectStorageMock);

		$this->subject->removeMaterial($material);

	}

	/**
	 * @test
	 */
	public function getColorReturnsInitialValueForColor()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getColor()
		);
	}

	/**
	 * @test
	 */
	public function setColorForObjectStorageContainingColorSetsColor()
	{
		$color = new \Webwaren\WwStep\Domain\Model\Color();
		$objectStorageHoldingExactlyOneColor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneColor->attach($color);
		$this->subject->setColor($objectStorageHoldingExactlyOneColor);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneColor,
			'color',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addColorToObjectStorageHoldingColor()
	{
		$color = new \Webwaren\WwStep\Domain\Model\Color();
		$colorObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$colorObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($color));
		$this->inject($this->subject, 'color', $colorObjectStorageMock);

		$this->subject->addColor($color);
	}

	/**
	 * @test
	 */
	public function removeColorFromObjectStorageHoldingColor()
	{
		$color = new \Webwaren\WwStep\Domain\Model\Color();
		$colorObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$colorObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($color));
		$this->inject($this->subject, 'color', $colorObjectStorageMock);

		$this->subject->removeColor($color);

	}

	/**
	 * @test
	 */
	public function getCollectionReturnsInitialValueForCollection()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getCollection()
		);
	}

	/**
	 * @test
	 */
	public function setCollectionForCollectionSetsCollection()
	{
		$collectionFixture = new \Webwaren\WwStep\Domain\Model\Collection();
		$this->subject->setCollection($collectionFixture);

		$this->assertAttributeEquals(
			$collectionFixture,
			'collection',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCountryReturnsInitialValueForCountry()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getCountry()
		);
	}

	/**
	 * @test
	 */
	public function setCountryForObjectStorageContainingCountrySetsCountry()
	{
		$country = new \Webwaren\WwStep\Domain\Model\Country();
		$objectStorageHoldingExactlyOneCountry = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneCountry->attach($country);
		$this->subject->setCountry($objectStorageHoldingExactlyOneCountry);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneCountry,
			'country',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addCountryToObjectStorageHoldingCountry()
	{
		$country = new \Webwaren\WwStep\Domain\Model\Country();
		$countryObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$countryObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($country));
		$this->inject($this->subject, 'country', $countryObjectStorageMock);

		$this->subject->addCountry($country);
	}

	/**
	 * @test
	 */
	public function removeCountryFromObjectStorageHoldingCountry()
	{
		$country = new \Webwaren\WwStep\Domain\Model\Country();
		$countryObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$countryObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($country));
		$this->inject($this->subject, 'country', $countryObjectStorageMock);

		$this->subject->removeCountry($country);

	}
}
