<?php

namespace Webwaren\WwStep\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Webwaren\WwStep\Domain\Model\Retail.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Regna Pera <pera@webwaren.ch>
 */
class RetailTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \Webwaren\WwStep\Domain\Model\Retail
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \Webwaren\WwStep\Domain\Model\Retail();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getNameReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getName()
		);
	}

	/**
	 * @test
	 */
	public function setNameForStringSetsName()
	{
		$this->subject->setName('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'name',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAdressReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAdress()
		);
	}

	/**
	 * @test
	 */
	public function setAdressForStringSetsAdress()
	{
		$this->subject->setAdress('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'adress',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTelephoneReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTelephone()
		);
	}

	/**
	 * @test
	 */
	public function setTelephoneForStringSetsTelephone()
	{
		$this->subject->setTelephone('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'telephone',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDescriptionReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDescription()
		);
	}

	/**
	 * @test
	 */
	public function setDescriptionForStringSetsDescription()
	{
		$this->subject->setDescription('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'description',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCoordinationxReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getCoordinationx()
		);
	}

	/**
	 * @test
	 */
	public function setCoordinationxForStringSetsCoordinationx()
	{
		$this->subject->setCoordinationx('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'coordinationx',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCoordinationyReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getCoordinationy()
		);
	}

	/**
	 * @test
	 */
	public function setCoordinationyForStringSetsCoordinationy()
	{
		$this->subject->setCoordinationy('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'coordinationy',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getRetailtypeReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setRetailtypeForIntSetsRetailtype()
	{	}

	/**
	 * @test
	 */
	public function getImagesReturnsInitialValueForFileReference()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getImages()
		);
	}

	/**
	 * @test
	 */
	public function setImagesForFileReferenceSetsImages()
	{
		$fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$this->subject->setImages($fileReferenceFixture);

		$this->assertAttributeEquals(
			$fileReferenceFixture,
			'images',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getCountryReturnsInitialValueForCountry()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getCountry()
		);
	}

	/**
	 * @test
	 */
	public function setCountryForObjectStorageContainingCountrySetsCountry()
	{
		$country = new \Webwaren\WwStep\Domain\Model\Country();
		$objectStorageHoldingExactlyOneCountry = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneCountry->attach($country);
		$this->subject->setCountry($objectStorageHoldingExactlyOneCountry);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneCountry,
			'country',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addCountryToObjectStorageHoldingCountry()
	{
		$country = new \Webwaren\WwStep\Domain\Model\Country();
		$countryObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$countryObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($country));
		$this->inject($this->subject, 'country', $countryObjectStorageMock);

		$this->subject->addCountry($country);
	}

	/**
	 * @test
	 */
	public function removeCountryFromObjectStorageHoldingCountry()
	{
		$country = new \Webwaren\WwStep\Domain\Model\Country();
		$countryObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$countryObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($country));
		$this->inject($this->subject, 'country', $countryObjectStorageMock);

		$this->subject->removeCountry($country);

	}

	/**
	 * @test
	 */
	public function getGroupReturnsInitialValueForGroup()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getGroup()
		);
	}

	/**
	 * @test
	 */
	public function setGroupForObjectStorageContainingGroupSetsGroup()
	{
		$group = new \Webwaren\WwStep\Domain\Model\Group();
		$objectStorageHoldingExactlyOneGroup = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneGroup->attach($group);
		$this->subject->setGroup($objectStorageHoldingExactlyOneGroup);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneGroup,
			'group',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addGroupToObjectStorageHoldingGroup()
	{
		$group = new \Webwaren\WwStep\Domain\Model\Group();
		$groupObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$groupObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($group));
		$this->inject($this->subject, 'group', $groupObjectStorageMock);

		$this->subject->addGroup($group);
	}

	/**
	 * @test
	 */
	public function removeGroupFromObjectStorageHoldingGroup()
	{
		$group = new \Webwaren\WwStep\Domain\Model\Group();
		$groupObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$groupObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($group));
		$this->inject($this->subject, 'group', $groupObjectStorageMock);

		$this->subject->removeGroup($group);

	}
}
