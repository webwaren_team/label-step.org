<?php
namespace Webwaren\WwStep\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Webwaren\WwStep\Controller\RetailController.
 *
 * @author Regna Pera <pera@webwaren.ch>
 */
class RetailControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \Webwaren\WwStep\Controller\RetailController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('Webwaren\\WwStep\\Controller\\RetailController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllRetailsFromRepositoryAndAssignsThemToView()
	{

		$allRetails = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$retailRepository = $this->getMock('Webwaren\\WwStep\\Domain\\Repository\\RetailRepository', array('findAll'), array(), '', FALSE);
		$retailRepository->expects($this->once())->method('findAll')->will($this->returnValue($allRetails));
		$this->inject($this->subject, 'retailRepository', $retailRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('retails', $allRetails);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenRetailToView()
	{
		$retail = new \Webwaren\WwStep\Domain\Model\Retail();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('retail', $retail);

		$this->subject->showAction($retail);
	}
}
