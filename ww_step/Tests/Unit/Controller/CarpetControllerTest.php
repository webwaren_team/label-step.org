<?php
namespace Webwaren\WwStep\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Webwaren\WwStep\Controller\CarpetController.
 *
 * @author Regna Pera <pera@webwaren.ch>
 */
class CarpetControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \Webwaren\WwStep\Controller\CarpetController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('Webwaren\\WwStep\\Controller\\CarpetController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllCarpetsFromRepositoryAndAssignsThemToView()
	{

		$allCarpets = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$carpetRepository = $this->getMock('Webwaren\\WwStep\\Domain\\Repository\\CarpetRepository', array('findAll'), array(), '', FALSE);
		$carpetRepository->expects($this->once())->method('findAll')->will($this->returnValue($allCarpets));
		$this->inject($this->subject, 'carpetRepository', $carpetRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('carpets', $allCarpets);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenCarpetToView()
	{
		$carpet = new \Webwaren\WwStep\Domain\Model\Carpet();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('carpet', $carpet);

		$this->subject->showAction($carpet);
	}
}
