<?php
/**
 * Created by PhpStorm.
 * User: pera
 * Date: 21.11.16
 * Time: 11:12
 */

namespace Webwaren\WwStep\ViewHelpers;


class GoogleChartsViewHelper  extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @return string
     * @param string $address
     * @param string $id
     * @param array $settings
     */
    public function render($address,$id='charts',$settings=''){

        $charts = " <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js?key=AIzaSyDcFCft5XAvpdxZ0DL1lvD3iU4OMEjkypE\"></script>
                   <script type=\"text/javascript\">
                          google.charts.load('current', {'packages':['geochart']},{'language': 'de'});
                          google.charts.setOnLoadCallback(drawRegionsMap);
                          
                           var jsonData = ".$address.";
                    
                          function drawRegionsMap() {
                            console.log(jsonData['country'].length);
                            var data = new google.visualization.DataTable();
                            data.addColumn('string', 'Country');
                            data.addColumn('string', 'Link');                 
                            
                            for(i=0;i<jsonData['country'].length;i++){
                                data.addRows([
                                    [jsonData['subtitle'][i],{v:jsonData['link'][i],f:jsonData['country'][i]}],
                                ]);
                            }     
                            
                                              
                            var view = new google.visualization.DataView(data);
                            view.hideColumns([1]);                 
                            
                            var options = {
                              colorAxis: {colors: 'white'},
                              backgroundColor: '#5acee0',       //ocean color
                              datalessRegionColor: '#e6665f',   //country color
                              defaultColor: 'black',
                              displayMode:'regions',
                              tooltip: {textStyle: {color: '#fff'}, trigger:'focus',isHtml: true},
                
                              legend:  'none'
                            };
                    
                            var chart = new google.visualization.GeoChart(document.getElementById('geochart-colors'));
                            google.visualization.events.addListener(chart, 'select', myClickHandler);
                            
                            chart.draw(data, options);
                            
                            function myClickHandler(){
                              var link = data.getValue(chart.getSelection()[0]['row'],1);
                              location.href = link;
                             }
                          };
                          
                          
                          
                        </script>
                         <div id=\"geochart-colors\" style=\"width: ".$settings['width']."; height:".$settings['height']."\"></div>
                         <style>
                            path {
                                  stroke: #555;     
                                }
                         </style>
                         ";

        return $charts;
    }

}
