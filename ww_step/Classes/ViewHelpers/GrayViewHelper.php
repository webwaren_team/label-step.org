<?php
/**
 * Created by PhpStorm.
 * User: pera
 * Date: 25.11.16
 * Time: 14:12
 */

namespace Webwaren\WwStep\ViewHelpers;


class GrayViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper{


    /**
     * @return mixed
     * @param string $path
     */
    public function render($path=""){

        $cobj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        $data['file'] = 'fileadmin'.$path;
        $data['file.']['width'] = 100;
        $data['file.']['height'] = 100;
        $data['file.']['params'] = '-type Grayscale';
        $re = $cobj->IMAGE($data);
        return $re;
    }
}