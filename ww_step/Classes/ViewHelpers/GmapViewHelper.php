<?php
namespace Webwaren\WwStep\ViewHelpers;

/**
 * Created by PhpStorm.
 * User: pera
 * Date: 14.11.16
 * Time: 09:23
 */
class GmapViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper{

    /**
     * @return string
     * @param array $address
     * @param string $id
     * @param array $settings
     * @param int $flag
     */
    public function render($address='',$id='map',$settings=array()){





        if($settings['map']['zoomdisable'] == 1){
            //Knöpfe aktivieren Ein-/Auszoomen
            $mapconfig = 'zoomControl:false,';
            //Zoomen deaktivieren
            $mapconfig .= 'disableDoubleClickZoom:true,';
            //Bewegung der Google Map deaktiveren
            $mapconfig .= 'draggable:true,';
            //Mit Mausrad aus/einzoomen
            $mapconfig .= 'scrollwheel:false,';
        }else{
            $mapconfig = '';
        }

        // 1= world map ; 15 = street view
        $zoom = $settings['map']['zoom'];
        $address = json_encode($address);

        $maps = "<script>
            var map, data, bounds, activeInfoWindow;
            var markers = [];

            function initMap(){
              bounds = new google.maps.LatLngBounds();
              data = " . $address . ";

              //Initizalize Google Map
              map = new google.maps.Map(document.getElementById('" . $id . "'),{
                zoom: " . $zoom . ",

                minZoom: 3,

                center: {lat: " . $settings['map']['latitude'] . ", lng: " . $settings['map']['longitude'] . "},
                //center: {lat: 46.8296375, lng: 8.4189101},

                //Street View
				streetViewControl:false,
				//Karte/Satellit
				mapTypeControl: false,
                //45 Grad ändern
				rotateControl:false,

				// disable zoom on mousescroll
				scrollwheel: false,

				" . $mapconfig . "
              });

              for (i = 0; i < data.length; i++){
                // Adds a marker at the center of the map.
                //addMarker(data[i][1],data[i][2],data[i][4],data[i][0],data[i][5]);
                addMarker(data[i]['latitude'],data[i]['longitude'],data[i]['images'],data[i]['info_window'],data[i]['retailType']);
              }

              //if has result only 1
              if(data.length === 1){
                map.setCenter(bounds.getCenter());
                map.setZoom(9);
              }else{
                map.setCenter(bounds.getCenter());
                map.fitBounds(bounds);
              }
               map.setOptions({styles: styles});
           }

            // Adds a marker to the map and push to the array.
            function addMarker(lat,lng,title,content,producer){
              if (typeof title == 'undefined' || !title){
                  contentString = content;
              }else{
                  contentString = title+'<br/>'+content;
              }

                  if(producer == 1){
                      var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat,lng),
                        map: map,
                        icon:'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
                      });
                  }else{
                      var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat,lng),
                        map: map,
                        icon:'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                      });
                  }

              var infowindow = new google.maps.InfoWindow({
                content: contentString
              });

              marker.addListener('click', function(){
                  activeInfoWindow&&activeInfoWindow.close();
                  infowindow.open(map, marker);
                  activeInfoWindow = infowindow;
              });
              markers.push(marker);

              bounds.extend(marker.getPosition());
            }

            // Sets the map on all markers in the array.
            function setMapOnAll(map){
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
              }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers(){
              setMapOnAll(null);
            }

            // Shows any markers currently in the array.
            function showMarkers(){
              setMapOnAll(map);
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
              clearMarkers();
              bakmarkers = markers;
              markers = [];
            }

            var styles = [
                            {
                                \"featureType\": \"administrative\",
                                \"elementType\": \"all\",
                                \"stylers\": [
                                    {
                                        \"visibility\": \"on\"
                                    },
                                    {
                                        \"weight\": \"1\"
                                    },
                                    {
                                        \"color\": \"#555555\"
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"administrative.country\",
                                \"elementType\": \"geometry\",
                                \"stylers\": [
                                    {
                                        \"saturation\": \"-100\"
                                    },
                                    {
                                        \"visibility\": \"on\"
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"landscape\",
                                \"elementType\": \"all\",
                                \"stylers\": [
                                    {
                                        \"visibility\": \"on\"
                                    },
                                    {
                                        \"color\": \"#E7665F\"
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"poi\",
                                \"elementType\": \"all\",
                                \"stylers\": [
                                    {
                                        \"visibility\": \"off\"
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"poi\",
                                \"elementType\": \"geometry\",
                                \"stylers\": [
                                    {
                                        \"color\": \"#f5f5f5\"
                                    },
                                    {
                                        \"lightness\": 21
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"road.highway\",
                                \"elementType\": \"geometry\",
                                \"stylers\": [
                                    {
                                        \"visibility\": \"off\"
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"road.highway\",
                                \"elementType\": \"geometry.fill\",
                                \"stylers\": [
                                    {
                                        \"color\": \"#fce0e0\"
                                    },
                                    {
                                        \"lightness\": 17
                                    },
                                    {
                                        \"visibility\": \"off\"
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"road.highway\",
                                \"elementType\": \"geometry.stroke\",
                                \"stylers\": [
                                    {
                                        \"color\": \"#000000\"
                                    },
                                    {
                                        \"lightness\": 29
                                    },
                                    {
                                        \"weight\": 0.2
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"road.arterial\",
                                \"elementType\": \"geometry\",
                                \"stylers\": [
                                    {
                                        \"color\": \"#000000\"
                                    },
                                    {
                                        \"lightness\": 18
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"road.local\",
                                \"elementType\": \"geometry\",
                                \"stylers\": [
                                    {
                                        \"color\": \"#000000\"
                                    },
                                    {
                                        \"lightness\": 16
                                    }
                                ]
                            },
                            {
                                \"featureType\": \"water\",
                                \"elementType\": \"all\",
                                \"stylers\": [
                                    {
                                        \"color\": \"#87C6D3\"
                                    }
                                ]
                            }
                        ];
        </script>
        <script async defer
            src=\"https://maps.googleapis.com/maps/api/js?language=".$settings['language']."&key=AIzaSyDcFCft5XAvpdxZ0DL1lvD3iU4OMEjkypE&callback=initMap\"></script>";

        $maps .= '<div id="'.$id.'"></div>';

        return $maps;
    }
}
