<?php
/**
 * Created by PhpStorm.
 * User: pera
 * Date: 25.11.16
 * Time: 14:12
 */

namespace Webwaren\WwStep\ViewHelpers;


class ContainsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper {


    /**
     * @return mixed
     * @param string $key
     * @param mixed $array
     */
    public function render($key='',$array=''){

        if(count($array) > 0 && is_array($array)){
            if (in_array($key, $array)){
                return $this->renderThenChild();
            }
        }
        return $this->renderElseChild();
    }
}