<?php
/**
 * Created by PhpStorm.
 * User: pera
 * Date: 30.11.16
 * Time: 10:05
 */

namespace Webwaren\WwStep\Utility;


class GeneralUtility{

    /**
     * get only retail of page
     *
     * @param array $PA
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     * @return string
     */
    public function render(array &$PA, $pObj) {
        $uid = $PA['row']['uid'];
        //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($PA['config']['foreign_table_where']);
        //$PA['config']['foreign_table_where'] = str_replace('###UID###', $uid, $PA['config']['foreign_table_where']);
        $PA['config']['foreign_table_where'] &= 'AND group > 0';
//        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($PA);
        return $PA;

    }

}
