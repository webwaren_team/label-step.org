<?php
namespace Webwaren\WwStep\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Collection
 */
class Collection extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';
    
    /**
     * description
     *
     * @var string
     */
    protected $description = '';
    
    /**
     * groups
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Group>
     * @cascade remove
     */
    protected $groups = null;

    /**
     * brands
     *
     * @var \Webwaren\WwStep\Domain\Model\Group
     * @cascade remove
     */
    protected $brands = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->groups = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Adds a Grop
     *
     * @param \Webwaren\WwStep\Domain\Model\Group $groups
     * @return void
     */
    public function addGroups(\Webwaren\WwStep\Domain\Model\Group $groups)
    {
        $this->groups->attach($groups);
    }
    
    /**
     * Removes a Groups
     *
     * @param \Webwaren\WwStep\Domain\Model\Group $groupsToRemove The Retail to be removed
     * @return void
     */
    public function removeGroups(\Webwaren\WwStep\Domain\Model\Group $groupsToRemove)
    {
        $this->groups->detach($groupsToRemove);
    }
    
    /**
     * Returns the groups
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Group> $groups
     */
    public function getGroups()
    {
        return $this->groups;
    }
    
    /**
     * Sets the groups
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Group> $groups
     * @return void
     */
    public function setGroups(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $groups)
    {
        $this->groups = $groups;
    }

    /**
     * Adds a brands
     *
     * @param \Webwaren\WwStep\Domain\Model\Group $brands
     * @return void
     */
    public function addBrands(\Webwaren\WwStep\Domain\Model\Group $brands)
    {
        $this->brands->attach($brands);
    }

    /**
     * Removes a brands
     *
     * @param \Webwaren\WwStep\Domain\Model\Group $brandsToRemove The Retail to be removed
     * @return void
     */
    public function removeBrands(\Webwaren\WwStep\Domain\Model\Group $brandsToRemove)
    {
        $this->brands->detach($brandsToRemove);
    }

    /**
     * Returns the brands
     *
     * @return \Webwaren\WwStep\Domain\Model\Group $brands
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * Sets the brands
     *
     * @param \Webwaren\WwStep\Domain\Model\Group $brands
     * @return void
     */
    public function setBrands(\Webwaren\WwStep\Domain\Model\Group $brands)
    {
        $this->brands = $brands;
    }

}