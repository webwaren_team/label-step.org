<?php
namespace Webwaren\WwStep\Domain\Model;

/**
 * Sessionstorage
 */
class SessionStorage implements \TYPO3\CMS\Core\SingletonInterface {

    const SESSIONNAMESPACE = 'tx_wwstep';

    /**
     * Returns the object stored in the user´s session
     * @param string $key
     * @return Object the stored object
     */
    public function get($key) {
        $sessionData = $this->getFrontendUser()->getKey('ses', self::SESSIONNAMESPACE.$key);
        return $sessionData;
    }

    /**
     * checks if object is stored in the user´s session
     * @param string $key
     * @return boolean
     */
    public function has($key) {
        $sessionData = $this->getFrontendUser()->getKey('ses', self::SESSIONNAMESPACE.$key);
        if ($sessionData == '') {
            return false;
        }
        return true;
    }

    /**
     * Writes something to storage
     * @param string $key
     * @param string $value
     * @return	void
     */
    public function set($key,$value) {
        $this->getFrontendUser()->setKey('ses', self::SESSIONNAMESPACE.$key, $value);
        $this->getFrontendUser()->storeSessionData();
    }

    /**
     * Writes a object to the session if the key is empty it used the classname
     * @param object $object
     * @param string $key
     * @return	void
     */
    public function storeObject($object,$key=null) {
        if (is_null($key)) {
            $key = get_class($object);
        }
        return $this->set($key,serialize($object));
    }

    /**
     * Writes something to storage
     * @param string $key
     * @return	object
     */
    public function getObject($key) {
        return unserialize($this->get($key));
    }

    /**
     * Cleans up the session: removes the stored object from the PHP session
     * @param string $key
     * @return	void
     */
    public function clean($key) {
        $this->getFrontendUser()->setKey('ses', self::SESSIONNAMESPACE.$key, NULL);
        $this->getFrontendUser()->storeSessionData();
    }

    /**
     * Gets a frontend user which is taken from the global registry or as fallback from TSFE->fe_user.
     *
     * @return	ux_tslib_feUserAuth	The current extended frontend user object
     * @throws	LogicException
     */
    public function getFrontendUser() {
        if ($GLOBALS ['TSFE']->fe_user) {
            return $GLOBALS ['TSFE']->fe_user;
        }
        throw new LogicException ( 'No Frontentuser found in session!' );
    }
}
