<?php
namespace Webwaren\WwStep\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Carpet
 */
class Carpet extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';
    
    /**
     * images
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $images = null;
    
    /**
     * art
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Art>
     * @cascade remove
     */
    protected $art = null;
    
    /**
     * material
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Material>
     * @cascade remove
     */
    protected $material = null;
    
    /**
     * color
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Color>
     * @cascade remove
     */
    protected $color = null;
    
    /**
     * collection
     *
     * @var \Webwaren\WwStep\Domain\Model\Collection
     */
    protected $collection = null;
    
    /**
     * country
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Country>
     * @cascade remove
     */
    protected $country = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->art = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->material = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->color = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->country = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->images = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function getImages()
    {
        return $this->images;
    }
    
    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     * @return void
     */
    public function setImages(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $images)
    {
        $this->images = $images;
    }
    
    /**
     * Adds a Art
     *
     * @param \Webwaren\WwStep\Domain\Model\Art $art
     * @return void
     */
    public function addArt(\Webwaren\WwStep\Domain\Model\Art $art)
    {
        $this->art->attach($art);
    }
    
    /**
     * Removes a Art
     *
     * @param \Webwaren\WwStep\Domain\Model\Art $artToRemove The Art to be removed
     * @return void
     */
    public function removeArt(\Webwaren\WwStep\Domain\Model\Art $artToRemove)
    {
        $this->art->detach($artToRemove);
    }
    
    /**
     * Returns the art
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Art> $art
     */
    public function getArt()
    {
        return $this->art;
    }
    
    /**
     * Sets the art
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Art> $art
     * @return void
     */
    public function setArt(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $art)
    {
        $this->art = $art;
    }
    
    /**
     * Adds a Material
     *
     * @param \Webwaren\WwStep\Domain\Model\Material $material
     * @return void
     */
    public function addMaterial(\Webwaren\WwStep\Domain\Model\Material $material)
    {
        $this->material->attach($material);
    }
    
    /**
     * Removes a Material
     *
     * @param \Webwaren\WwStep\Domain\Model\Material $materialToRemove The Material to be removed
     * @return void
     */
    public function removeMaterial(\Webwaren\WwStep\Domain\Model\Material $materialToRemove)
    {
        $this->material->detach($materialToRemove);
    }
    
    /**
     * Returns the material
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Material> $material
     */
    public function getMaterial()
    {
        return $this->material;
    }
    
    /**
     * Sets the material
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Material> $material
     * @return void
     */
    public function setMaterial(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $material)
    {
        $this->material = $material;
    }
    
    /**
     * Adds a Color
     *
     * @param \Webwaren\WwStep\Domain\Model\Color $color
     * @return void
     */
    public function addColor(\Webwaren\WwStep\Domain\Model\Color $color)
    {
        $this->color->attach($color);
    }
    
    /**
     * Removes a Color
     *
     * @param \Webwaren\WwStep\Domain\Model\Color $colorToRemove The Color to be removed
     * @return void
     */
    public function removeColor(\Webwaren\WwStep\Domain\Model\Color $colorToRemove)
    {
        $this->color->detach($colorToRemove);
    }
    
    /**
     * Returns the color
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Color> $color
     */
    public function getColor()
    {
        return $this->color;
    }
    
    /**
     * Sets the color
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Color> $color
     * @return void
     */
    public function setColor(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $color)
    {
        $this->color = $color;
    }
    
    /**
     * Returns the collection
     *
     * @return \Webwaren\WwStep\Domain\Model\Collection $collection
     */
    public function getCollection()
    {
        return $this->collection;
    }
    
    /**
     * Sets the collection
     *
     * @param \Webwaren\WwStep\Domain\Model\Collection $collection
     * @return void
     */
    public function setCollection(\Webwaren\WwStep\Domain\Model\Collection $collection)
    {
        $this->collection = $collection;
    }
    
    /**
     * Adds a Country
     *
     * @param \Webwaren\WwStep\Domain\Model\Country $country
     * @return void
     */
    public function addCountry(\Webwaren\WwStep\Domain\Model\Country $country)
    {
        $this->country->attach($country);
    }
    
    /**
     * Removes a Country
     *
     * @param \Webwaren\WwStep\Domain\Model\Country $countryToRemove The Country to be removed
     * @return void
     */
    public function removeCountry(\Webwaren\WwStep\Domain\Model\Country $countryToRemove)
    {
        $this->country->detach($countryToRemove);
    }
    
    /**
     * Returns the country
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Country> $country
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Sets the country
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Country> $country
     * @return void
     */
    public function setCountry(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $country)
    {
        $this->country = $country;
    }

}