<?php
namespace Webwaren\WwStep\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Group
 */
class Group extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * brands
     *
     * @var string
     */
    protected $brands;

    /**
     * show Page
     *
     * @var string
     */
    protected $showPage;

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the showPage
     *
     * @return string $showPage
     */
    public function getShowPage()
    {
        return $this->showPage;
    }

    /**
     * Sets the showPage
     *
     * @param string $showPage
     * @return void
     */
    public function setShowPage($showPage)
    {
        $this->showPage = $showPage;
    }

    /**
     * Returns the brands
     *
     * @return string $brands
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * Sets the brands
     *
     * @param string $brands
     * @return void
     */
    public function setBrands($brands){
        $this->brands = $brands;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}
