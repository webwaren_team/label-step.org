<?php
namespace Webwaren\WwStep\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Retail
 */
class Retail extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{


    /**
     * infoWindow
     *
     * @var string
     */
    protected $infoWindow;

    /**
     * configurationMap
     *
     * @var string
     */
    protected $title;

    /**
     * configurationMap
     *
     * @var string
     */
    protected $configurationMap;

    /**
     * latitude
     *
     * @var float
     */
    protected $latitude;
    /**
     * longitude
     *
     * @var float
     */
    protected $longitude;
    
    /**
     * retailtype
     *
     * @var int
     */
    protected $retailtype = 0;

    /**
     * images
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $images = null;
    
    /**
     * country
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Country>
     * @cascade remove
     */
    protected $country = null;
    
    /**
     * groups
     *
     * @var \Webwaren\WwStep\Domain\Model\Group
     * @cascade remove
     */
    protected $groups = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->country = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the infoWindow
     *
     * @return string $infoWindow
     */
    public function getInfoWindow()
    {
        return $this->infoWindow;
    }

    /**
     * Sets the infoWindow
     *
     * @param string $infoWindow
     * @return void
     */
    public function setInfoWindow($infoWindow)
    {
        $this->infoWindow = $infoWindow;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the configurationMap
     *
     * @return string $configurationMap
     */
    public function getConfigurationMap()
    {
        return $this->configurationMap;
    }

    /**
     * Sets the configurationMap
     *
     * @param string $configurationMap
     * @return void
     */
    public function setConfigurationMap($configurationMap)
    {
        $this->configurationMap = $configurationMap;
    }

    /**
     * Returns the latitude
     *
     * @return float $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude
     *
     * @param float $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Returns the longitude
     *
     * @return float $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude
     *
     * @param float $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
    
    /**
     * Returns the retailtype
     *
     * @return int $retailtype
     */
    public function getRetailtype()
    {
        return $this->retailtype;
    }
    
    /**
     * Sets the retailtype
     *
     * @param int $retailtype
     * @return void
     */
    public function setRetailtype($retailtype)
    {
        $this->retailtype = $retailtype;
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     * @return void
     */
    public function setImages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $images)
    {
        $this->images = $images;
    }
    
    /**
     * Adds a Country
     *
     * @param \Webwaren\WwStep\Domain\Model\Country $country
     * @return void
     */
    public function addCountry(\Webwaren\WwStep\Domain\Model\Country $country)
    {
        $this->country->attach($country);
    }
    
    /**
     * Removes a Country
     *
     * @param \Webwaren\WwStep\Domain\Model\Country $countryToRemove The Country to be removed
     * @return void
     */
    public function removeCountry(\Webwaren\WwStep\Domain\Model\Country $countryToRemove)
    {
        $this->country->detach($countryToRemove);
    }
    
    /**
     * Returns the country
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Country> $country
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Sets the country
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Country> $country
     * @return void
     */
    public function setCountry(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $country)
    {
        $this->country = $country;
    }
    
    /**
     * Returns the groups
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Group> $groups
     */
    public function getGroups()
    {
        return $this->groups;
    }
    
    /**
     * Sets the group
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Webwaren\WwStep\Domain\Model\Group> $groups
     * @return void
     */
    public function setGroups(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $groups)
    {
        $this->groups = $groups;
    }

}