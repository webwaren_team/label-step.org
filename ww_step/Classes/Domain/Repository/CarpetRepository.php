<?php
namespace Webwaren\WwStep\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Carpets
 */
class CarpetRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    public function initializeObject(){
        // get the settings
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        // modify the settings
        $querySettings->setIgnoreEnableFields(false);
        $querySettings->setRespectStoragePage(false);

        // store the settings as default-values
        $this->setDefaultQuerySettings($querySettings);
    }


    public function getByCollection(\Webwaren\WwStep\Domain\Model\Collection $collection){
        $query = $this->createQuery();

        $query->matching(
            $query->equals('collection', $collection)
        );
        return $query->execute();
    }

    public function getByUids(array $carpetuids){
        $query = $this->createQuery();
        $query->setOrderings($this->orderByField('uid', $carpetuids));

        $query->matching(
            $query->in('uid', $carpetuids)
        );

        return $query->execute();
    }

    /**
     *  Set the order method
     *
     */
    protected function orderByField($field, $values) {
        $orderings = array();
        foreach ($values as $value) {
            $orderings["$field={$value}"] = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
        }
        return $orderings;
    }

    public function getByCollections(\TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryresult, $search, $sortfield = null, $sortmode = null){

        $query = $this->createQuery();

        $sorting = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING;

        if ($sortmode == 'desc') {
            $sorting = \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING;
        }

        if ($sortfield) {
           $query->setOrderings(
                array($sortfield => $sorting)
            );

        }

        $constraints = array();

        if($search['collection']){
            $constraints[] = $query->equals('collection',$search['collection']);
        } else {
            $constraints[] = $query->in('collection', $queryresult);
        }

        //country
        if($search['filter']['country']){
            if(is_array($search['filter']['country'])){
                $con = array();
                foreach($search['filter']['country'] as $country){
                    $con[] = $query->contains('country',$country);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('country',$search['filter']['country']);
            }
        }

        //art
        if($search['filter']['art']){
            if(is_array($search['filter']['art'])){
                $con = array();
                foreach($search['filter']['art'] as $art){
                    $con[] = $query->contains('art',$art);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('art',$search['filter']['art']);
            }
        }

        //material
        if($search['filter']['material']){
            if(is_array($search['filter']['material'])){
                $con = array();
                foreach($search['filter']['material'] as $material){
                    $con[] = $query->contains('material',$material);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('material',$search['filter']['material']);
            }
        }

        //color
        if($search['filter']['color']){
            if(is_array($search['filter']['color'])){
                $con = array();
                foreach($search['filter']['color'] as $color){
                    $con[] = $query->contains('color',$color);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('color',$search['filter']['color']);
            }
        }

        if(count($constraints) > 0){
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        return $query->execute();
    }

    public function findBySearch($search){
        $query = $this->createQuery();

        $constraints = array();

        if($search['collection']){
            $constraints[] = $query->equals('collection',$search['collection']);
        }

        if($search['partner']){
           if(!$search['collection']){
                $constraints[] = $query->logicalOr(
                    $query->equals('collection.groups',$search['partner']),
                    $query->equals('collection.brands',$search['partner'])
                );
            }
        }

        //country
        if($search['filter']['country']){
            if(is_array($search['filter']['country'])){
                $con = array();
                foreach($search['filter']['country'] as $country){
                    $con[] = $query->contains('country',$country);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('country',$search['filter']['country']);
            }
        }

        //art
        if($search['filter']['art']){
            if(is_array($search['filter']['art'])){
                $con = array();
                foreach($search['filter']['art'] as $art){
                    $con[] = $query->contains('art',$art);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('art',$search['filter']['art']);
            }
        }

        //material
        if($search['filter']['material']){
            if(is_array($search['filter']['material'])){
                $con = array();
                foreach($search['filter']['material'] as $material){
                    $con[] = $query->contains('material',$material);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('material',$search['filter']['material']);
            }
        }

        //color
        if($search['filter']['color']){
            if(is_array($search['filter']['color'])){
                $con = array();
                foreach($search['filter']['color'] as $color){
                    $con[] = $query->contains('color',$color);
                }
                $constraints[] = $query->logicalOr($con);
            }else{
                $constraints[] = $query->contains('color',$search['filter']['color']);
            }
        }

        if(count($constraints) > 0){
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        return $query->execute();
    }
}
