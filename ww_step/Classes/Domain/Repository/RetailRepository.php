<?php
namespace Webwaren\WwStep\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Retails
 */
class RetailRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    /**
     * @var null
     */
    protected $settings = null;

    public function initializeObject() {
        // get the settings
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        // modify the settings
        $querySettings->setIgnoreEnableFields(false);
        $querySettings->setRespectStoragePage(false);

        // store the settings as default-values
        $this->setDefaultQuerySettings($querySettings);

        /** access to settings **/
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
        $this->settings = $configurationManager->getConfiguration( \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

    }

    /**
     * find all Retailpages
     *
     * @return mixed
     */
    public function findAll($read = false){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->equals("hidden", "0"),
                $query->equals("deleted", "0"),
                $query->equals("pid", $this->settings['retailPID'])
            )
        );

        if($read){
            $newRes = $this->getFields($query->execute()->toArray());
            return $newRes;
        }

        return $query->execute();
    }

    /**
     * find retail by keyword
     *
     * @return mixed
     */
    public function searchRetail($keywords){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->logicalAnd(
                    $query->equals("hidden", "0"),
                    $query->equals("deleted", "0"),
                    $query->equals("pid", $this->settings['retailPID'])
                ),
                $query->logicalOr(
                    $query->like("title", '%'.$keywords.'%'),
                    $query->like("infoWindow",'%'.$keywords.'%')
                )
            )
        );
        $newRes = $this->getFields($query->execute()->toArray());
        return $newRes;
    }

    /**
     * find retail by keyword
     *
     * @return mixed
     */
    public function searchRetailGroup($group){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->logicalAnd(
                    $query->equals("hidden", "0"),
                    $query->equals("deleted", "0"),
                    $query->equals("groups", $group)
                )
            )
        );
        $newRes = $this->getFields($query->execute()->toArray());

        return $newRes;
    }




    /**
     * find retail by retailgroup
     *
     * @return mixed
     */
    public function getCompany($group){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->equals("hidden", "0"),
                $query->equals("deleted", "0"),
                $query->equals('groups', $group),
                $query->equals("pid", $this->settings['retailPID'])
            )
        );
        return $query->execute()->getFirst();
    }

    /**
     * find retail by retailgroup
     *
     * @return mixed
     */
    public function getCompanyShow($group){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->equals("hidden", "0"),
                $query->equals("deleted", "0"),
                $query->equals('groups', $group),
//                $query->greaterThan('showPage', "0"),
                $query->equals("pid", $this->settings['retailPID'])
            )
        );

        $result =  $query->execute();

        if($result->count() > 1) {
            $groupView['uid'] = 27;
            $groupView['group'] = $group;
            return $groupView;
        }else{
            return $query->execute()->getFirst();
        }

    }

    /**
     * find retail by retailgroup
     *
     * @return mixed
     */
    public function getCompanyShowCarpet($group){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->equals("hidden", "0"),
                $query->equals("deleted", "0"),
                $query->equals('groups', $group),
//                $query->greaterThan('showPage', "0"),
                $query->equals("pid", $this->settings['retailPID'])
            )
        );

        return $query->execute()->getFirst();
    }


    /**
     * find retail by filter
     *
     * @return mixed
     */
    public function filterResults($country=null,$company=null,$type=null){

        $query = $this->createQuery();

        $constraints[] = $query->logicalAnd(
            $query->equals("hidden", "0"),
            $query->equals("deleted", "0"),
            $query->equals("pid", $this->settings['retailPID'])
        );

        if(isset($country) && !empty($country)){
            $constraints[] = $query->equals("country", $country);
        }

        if(isset($company) && !empty($company)){
            $constraints[] =  $query->equals('groups', $company);
        }
        if(isset($type)){
            $constraints[] =  $query->equals("retailtype", $type);
        }

        $query->matching(
            $query->logicalAnd($constraints)
        );

        $newRes = $this->getFields($query->execute()->toArray());
        return $newRes;
    }

    /**
     * get only needed fields
     *
     * @param $res
     * @return array
     */
    protected function getFields($res){
        foreach($res as $result){
            $temp['info_window'] = $result->getInfoWindow();
            $temp['latitude'] = $result->getLatitude();
            $temp['longitude'] = $result->getLongitude();
            $temp['uid'] = $result->getUid();
            $temp['images'] = $result->getImages();
            $temp['retailType'] = $result->getRetailtype();
            $temp['title'] = $result->getTitle();
            $temp['language'] = 99;
            $newRes[] = $temp;
            $temp = array();
        }
        return $newRes;
    }

}
