<?php
namespace Webwaren\WwStep\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Collections
 */
class CollectionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    public function initializeObject() {
        // get the settings
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        // modify the settings
        $querySettings->setIgnoreEnableFields(false);
        $querySettings->setRespectStoragePage(false);
        // store the settings as default-values
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );


    public function getByGroup(\Webwaren\WwStep\Domain\Model\Group $group){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalOr(
                $query->contains('groups', $group),
                $query->equals('brands', $group)
            )
        );
        return $query->execute();
    }

    public function getByPartnerId($partner){
        $query = $this->createQuery();

        $sql_statement = 'SELECT tx_wwstep_domain_model_collection.* FROM tx_wwstep_domain_model_collection
                          LEFT OUTER JOIN tx_wwstep_domain_model_group_mm ON tx_wwstep_domain_model_collection.uid = tx_wwstep_domain_model_group_mm.uid_local
                          WHERE (tx_wwstep_domain_model_collection.brands = ' . $partner .' OR tx_wwstep_domain_model_group_mm.uid_foreign = ' . $partner . ')  
                            AND tx_wwstep_domain_model_collection.deleted=0
                            AND tx_wwstep_domain_model_collection.t3ver_state<=0
                            AND tx_wwstep_domain_model_collection.pid<>-1
                            AND tx_wwstep_domain_model_collection.hidden=0
                            AND tx_wwstep_domain_model_collection.starttime<='.time().'
                            AND (tx_wwstep_domain_model_collection.endtime=0 OR tx_wwstep_domain_model_collection.endtime>'.time().') ';



        $query->statement($sql_statement);

        return $query->execute();
    }

}
