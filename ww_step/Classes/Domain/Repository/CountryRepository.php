<?php
namespace Webwaren\WwStep\Domain\Repository;


    /***************************************************************
     *
     *  Copyright notice
     *
     *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * The repository for Retails
 */
class CountryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository{

    protected $defaultOrderings = array(
        'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    public function initializeObject() {
        // get the settings
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        // modify the settings
        $querySettings->setIgnoreEnableFields(false);
        $querySettings->setRespectStoragePage(false);

        // store the settings as default-values
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * type: 1= Country for producing, 0=country for distribution
     * @return mixed
     */
    public function getAllCountry($type=1){
        $query = $this->createQuery();

        $query->matching(
            $query->logicalAnd(
                $query->equals("hidden", "0"),
                $query->equals("deleted", "0"),
                $query->equals("producer", $type)
            )
        );
        return $query->execute();
    }
}
