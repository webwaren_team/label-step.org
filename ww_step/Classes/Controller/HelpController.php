<?php
/**
 * Created by PhpStorm.
 * User: pera
 * Date: 05.12.16
 * Time: 14:26
 */

namespace Webwaren\WwStep\Controller;


use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Backend\View\BackendTemplateView;

class HelpController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {


    /**
     * Default View Container
     *
     * @var BackendTemplateView
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * Initialize the view
     *
     * @param ViewInterface $view The view
     * @return void
     */
    public function initializeView(ViewInterface $view)
    {
        /** @var BackendTemplateView $view */
        parent::initializeView($view);
    }

    public function indexAction(){
        $this->view->assign('index', '');
    }

}