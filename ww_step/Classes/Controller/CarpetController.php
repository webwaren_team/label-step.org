<?php
namespace Webwaren\WwStep\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * CarpetController
 */
class CarpetController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * carpetRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\CarpetRepository
     * @inject
     */
    protected $carpetRepository = NULL;


    /**
     * collectionRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\CollectionRepository
     * @inject
     */
    protected $collectionRepository = NULL;

    /**
     * retailRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\RetailRepository
     * @inject
     */
    protected $retailRepository = NULL;

    /**
     * countryRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = NULL;

    /**
     * artRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\ArtRepository
     * @inject
     */
    protected $artRepository = NULL;

    /**
     * materialRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\MaterialRepository
     * @inject
     */
    protected $materialRepository = NULL;

    /**
     * colorRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\ColorRepository
     * @inject
     */
    protected $colorRepository = NULL;

    /**
     * groupRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\GroupRepository
     * @inject
     */
    protected $groupRepository = NULL;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        // search options
        $this->searchOption($search=array());

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->session = $objectManager ->get('Webwaren\\WwStep\\Domain\\Model\\SessionStorage');

        if (!$this->session->get('carpetids-list')){
            $carpets = $this->carpetRepository->findAll()->toArray();
            shuffle($carpets);

            $carpetids = '';
            foreach ($carpets as $carpet){
                $carpetids .= $carpet->getUid() . ',';
            }

            $carpetids = substr($carpetids, 0,strlen($carpetids)-1);
            $this->session->set('carpetids-list', $carpetids);

        } else {
            $carpetids = $this->session->get('carpetids-list');
            $carpetids = explode(',',$carpetids);
            $carpets = $this->carpetRepository->getByUids($carpetids);
        }

        $this->view->assign('carpets', $carpets);
    }

    /**
     * list Result
     *
     * @return void
     */
    public function listRetailAction(){
        $retail = $this->retailRepository->findByUid($_GET['id']);
        $groups = $retail->getGroups();

        $carpets = array();

        if($groups != null){
            $collections = $this->collectionRepository->getByGroup($groups);
            foreach ($collections as $collection){
                $temp = $this->carpetRepository->getByCollection($collection);
                foreach ($temp as $item) {
                    $carpets[] = $item;
                }

            }
        }

        $this->view->assign('carpets', $carpets);
    }

    /**
     * list Resultsearch
     *
     * @return void
     */
    public function listSearchResultsAction(){

        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->session = $objectManager ->get('Webwaren\\WwStep\\Domain\\Model\\SessionStorage');

        $search = $this->request->getArguments();

        // search options
        $this->searchOption($search);

        $this->view->assign('filterPartner',$search['partner']);
        $this->view->assign('filterCollection',$search['collection']);
        $this->view->assign('filter',$search['filter']);


        $sessionkey = 'carpetids-list-p' .
            $search['partner'] .'-c' .$search['collection'] . '-fc' .
            $search['filter']['country']. '-fa' .
            $search['filter']['art']. '-fm' .
            $search['filter']['material'] . '-fc' .
            $search['filter']['color'];

        if (!$this->session->get($sessionkey)) {
            if ($search['partner']) {
                $collections = $this->collectionRepository->getByPartnerId($search['partner']);
                if ($collections->toArray()) {
                    $carpets = $this->carpetRepository->getByCollections($collections, $search,
                        $this->settings['sortfield'], $this->settings['sortmode']);
                }

            } else {
                $carpets = $this->carpetRepository->findBySearch($search);
            }

            if ($carpets){
                $carpets = $carpets->toArray();
                shuffle($carpets);
            }

            $carpetids = '';
            foreach ($carpets as $carpet){
                $carpetids .= $carpet->getUid() . ',';
            }

            $carpetids = substr($carpetids, 0,strlen($carpetids)-1);
            $this->session->set($sessionkey, $carpetids);


        } else {
            $carpetids = $this->session->get($sessionkey);
            $carpetids = explode(',',$carpetids);
            $carpets = $this->carpetRepository->getByUids($carpetids);

        }

        $this->view->assign('carpets', $carpets);
    }

    /**
     * Debugs a SQL query from a QueryResult
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult
     * @param boolean $explainOutput
     * @return void
     */
    public function debugQuery(\TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult, $explainOutput = FALSE){

        $GLOBALS['TYPO3_DB']->debugOutput = 2;

        if($explainOutput){
            $GLOBALS['TYPO3_DB']->explainOutput = false;
        }

        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;

        $queryResult->toArray();

        //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($queryResult->toArray());
        //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($GLOBALS['TYPO3_DB']->debug_lastBuiltQuery);

        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = false;
        $GLOBALS['TYPO3_DB']->explainOutput = false;
        $GLOBALS['TYPO3_DB']->debugOutput = false;
    }


    /**
     * action show
     *
     * @param \Webwaren\WwStep\Domain\Model\Carpet $carpet
     * @return void
     */
    public function showAction(\Webwaren\WwStep\Domain\Model\Carpet $carpet = null)
    {
        $collection = $carpet->getCollection();

        $GLOBALS['TSFE']->page['title'] = $carpet->getTitle();
        $GLOBALS['TSFE']->indexedDocTitle = $carpet->getTitle();

        $retail = $this->retailRepository->getCompanyShowCarpet($collection->getBrands());
        $retail2 = $this->retailRepository->getCompanyShow($collection->getBrands());

        $groups = $this->groupRepository->findAll();

        $this->view->assign('group',$groups);
        $this->view->assign('brands',$retail);
        $this->view->assign('brands2',$retail2);
        $this->view->assign('carpet', $carpet);
    }

    /**
     * search options
     */
    protected function searchOption($search){
        $partner = $this->groupRepository->findAll();
//        $collection = $this->collectionRepository->findAll();
        if($search['partner']) {
            //$grp = $this->groupRepository->findByUid($search['partner']);
            $collection = $this->collectionRepository->getByPartnerId($search['partner']);

        }
        $country = $this->countryRepository->getAllCountry();
        $art = $this->artRepository->findAll();
        $material = $this->materialRepository->findAll();
        $color = $this->colorRepository->findAll();

        $this->view->assign('partner',$partner);
        $this->view->assign('collection',$collection);
        $this->view->assign('countries',$country);
        $this->view->assign('arts',$art);
        $this->view->assign('materials',$material);
        $this->view->assign('colors',$color);

        $carpets['country'] = $country;
    }
}
