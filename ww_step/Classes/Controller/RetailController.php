<?php
namespace Webwaren\WwStep\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * RetailController
 */
class RetailController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * retailRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\RetailRepository
     * @inject
     */
    protected $retailRepository = NULL;

    /**
     * countryRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = NULL;

    /**
     * groupRepository
     *
     * @var \Webwaren\WwStep\Domain\Repository\GroupRepository
     * @inject
     */
    protected $groupRepository = NULL;



    /**
     * action list
     *
     * @return void
     */
    public function listAction(){
        $argument = $this->request->getArguments();

        //Ajax-Call
        if(isset($_POST['data'])){
            $result = $this->filterRetailAction($_POST);
            return json_encode($result);
        }


        if($argument['group']) {
            $result = $this->retailRepository->searchRetailGroup($argument['group']);
        }

        if($this->settings['group']) {
            $result = $this->retailRepository->searchRetailGroup($this->settings['group']);
        }

        if($argument['searchKey']){
            $result = $this->retailRepository->searchRetail($argument['searchKey']);
        }

        //selectfilter
        $country = $this->countryRepository->getAllCountry(0);
        $group = $this->groupRepository->findAll();

        $this->view->assign('country', $country);
        $this->view->assign('group', $group);

        $this->view->assign('filterCountry',$argument['group']);
        $this->view->assign('filterGroup',$argument['group']);

        //if result empty, all
        if(empty($result)){
            $result = $this->retailRepository->findAll(true);
        }
        $result = $this->makeReadable($result);
        $this->view->assign('retails', $result);
    }

    /**
     * Google Charts: list all Producer (Country)
     *
     * @return void
     */
    public function listProducerAction(){
        //echo $this->uriBuilder->reset()->setCreateAbsoluteUri(TRUE)->setArguments(array('id'=>13))->build();

        $pages = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
        $pages->init($show_hidden = -1);
        $pages->enableFields('pages', $show_hidden = -1, $ignore_array = array(),$noVersionPreview = false);
        $pages->enableFields('pages_language_overlay', $show_hidden = -1, $ignore_array = array(),$noVersionPreview = false);


        $countries = $pages->getMenu($this->settings['countryPID'],'title,uid,subtitle');

        // if not default language get localized records
        if ($countries && $GLOBALS['TSFE']->sys_language_uid > 0) {
            $countries = $pages->getPagesOverlay($countries, $GLOBALS['TSFE']->sys_language_uid);
        }


        $producer = array();
        foreach ($countries as $country){
            
            $producer['country'][] = $country['title'];
            $producer['subtitle'][] = $country['subtitle'];
            $producer['link'][] = $GLOBALS['TSFE']->baseUrl.'index.php?id='.$country['uid'].'&L='.$GLOBALS['TSFE']->sys_language_uid;
        }
        //$producer = implode(',',$producer);
        $producer =  json_encode($producer);
        $this->view->assign('producer', $producer);
    }

    /**
     * list all Retails (pages)
     *
     * @return void
     */
    public function listRetailAction(){

        $argument = $this->request->getArguments();


        $this->view->assign('filterCountry',$argument['country']);
        $this->view->assign('filterGroup',$argument['group']);

        //filter
        if($argument['country']){
            $c = $this->countryRepository->findByUid($argument['country']);
            $this->view->assign('filterCountryName',$c->getName());
        }
        if($argument['group']){
            $g = $this->groupRepository->findByUid($argument['group']);
        }
        $result = $this->retailRepository->filterResults($c,$g);

        $country = $this->countryRepository->getAllCountry(0);
        $group = $this->groupRepository->findAll();
        $this->view->assign('country', $country);
        $this->view->assign('group', $group);

        //if result empty, all
        if(empty($result)){
            $result = $this->retailRepository->findAll(true);
        }
        $result = $this->makeReadable($result);

        $this->view->assign('retails', $result);
    }


    public function filterRetailAction($data){
        if(isset($data['searchKey'])){
            $result = $this->retailRepository->searchRetail($data['searchKey']);
            return $this->makeReadable($result);
        }

        if(!isset($data['retailType'])){
            $data['retailType'] = null;
        }
        $result = $this->retailRepository->filterResults(intval($data['country']),intval($data['group']),$data['retailType']);
        return $this->makeReadable($result);

    }


    /**
     * Google Map: search by Retail
     *
     * @return mixed
     */
    public function searchRetailerAction(){
        if($_REQUEST['reset']){
            $result = $this->retailRepository->findAll(true);
            $result = $this->makeReadable($result);
            return json_encode($result);
        }
        $data = $_POST['data'];
        $result = $this->retailRepository->searchRetail($data);
        $result = $this->makeReadable($result);
        return json_encode($result);
    }

    /**
     * Show Google Map Retail
     *
     * @return mixed
     */
    public function showMapAction(){
        $page = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
        $page = $page->getPage($_GET['id']);

//        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->settings['longitude']);

        $lat = $page['latitude'];
        $lng = $page['longitude'];

        if(preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $this->settings['latitude']) && !preg_match('/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/', $this->settings['longitude'])){
            $lat = $this->settings['latitude'];
            $lng = $this->settings['longitude'];
        }

        $map = array('latitude'=> $lat, 'longitude'=>$lng, 'apiKey' => $this->settings['apiKey']);
        $this->view->assign('map', $map);
    }

//&language=ja&region=JP
    /**
     * make google map friendly
     *
     * @return mixed
     */
    private function makeReadable($arr){
        if(!$arr){
            $arr = $this->retailRepository->findAll(true);
        }

        foreach ($arr as $key =>$val){
            $tempArr = array();
            foreach ($val as $kkey => $v){

                if(is_object($v)){
                    $temp = '<img src="'.$GLOBALS['TSFE']->baseUrl.'fileadmin'.$v->getOriginalResource()->getIdentifier().'" height="50px"/>';
                    $tempArr[$kkey] ='<a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?id='.$val['uid'].'&L='.$GLOBALS['TSFE']->sys_language_uid.'">'.$temp.'</a>';
                }else{
                    $tempArr[$kkey] = $v;
                }
            }
            $array[]= $tempArr;
        }
        return $array;
    }

}
