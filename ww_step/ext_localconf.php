<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

// Step Teppich Gallery
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Webwaren.' . $_EXTKEY,
	'Pi1',
	array(
		'Carpet' => 'list, show, listRetail, listSearchResults',
		
	),
	// non-cacheable actions
	array(
		'Carpet' => 'list, show, listRetail, listSearchResults',

	)
);


// Step Google Map
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Webwaren.' . $_EXTKEY,
	'Pi2',
	array(
		'Retail' => 'list,searchRetailer,listProducer,filterResult,filterRetail,showMap',
		
	),
	// non-cacheable actions
	array(
		'Retail' => 'list,searchRetailer,listProducer,filterResult,filterRetail,showMap',
		
	)
);

// Step Händlerverzeichnis
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Webwaren.' . $_EXTKEY,
	'Pi3',
	array(
		'Retail' => 'listRetail,filterResult,filterListRetail',

	),
	// non-cacheable actions
	array(
		'Retail' => 'listRetail,filterResult,filterListRetail',

	)
);

