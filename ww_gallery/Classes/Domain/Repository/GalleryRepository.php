<?php
namespace WwGallery\WwGallery\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Galleries
 */
class GalleryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    public function getCategory($fileuid=NULL){
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->statement("SELECT uid_local FROM sys_category_record_mm 
                                                                    INNER JOIN sys_category 
                                                                    ON sys_category.uid = uid_local  
                                                                    INNER JOIN sys_file_metadata
                                                                    ON sys_file_metadata.uid = uid_foreign
                                                           WHERE uid_foreign = 70")->execute();
        return $query;
    }
}