<?php
namespace WwGallery\WwGallery\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Regna Pera <pera@webwaren.ch>, Webwaren GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * GalleryController
 */
class GalleryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * galleryRepository
     *
     * @var \WwGallery\WwGallery\Domain\Repository\GalleryRepository
     * @inject
     */
    protected $galleryRepository = NULL;

    /**
     * action list
     *
     * @return void
     */
    public function listAction(){
        $collection = $this->getMediaByCategory($this->settings['categoriesList']);

        $reference = array();
        if (count($collection) > 0) {
            foreach ($collection as $item) {
                $reference[] = $this->getImage($item['file']);
            }
        }
        $this->view->assign('image',$this->settings['image']);
        $this->view->assign('galleries', $reference);
    }


    /**
     * folder action
     *
     * @return void
     */
    public function listFolderAction(){
        $folder = explode(':',$this->settings['folder'],2);
        $images = $this->getImages($folder[1]);

        $this->view->assign('image',$this->settings['image']);
        $this->view->assign('galleries', $images);
    }

    private function getImage($uid){
        $img = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance()->getFileObject($uid);
        return $img;
    }

    private function getImages($identifier){
        $folder = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance()->getFolderObjectFromCombinedIdentifier($identifier);
        $files = $folder->getFiles();
        return $files;
    }

    private function getMediaByCategory($catUid,$tbl='sys_file_metadata'){
        $catUid = explode(',',$this->settings['categoriesList']);

        //fetch records
        $tempCol = array();
        foreach($catUid as $uid){
            $collection = \TYPO3\CMS\Frontend\Category\Collection\CategoryCollection::load(
                $uid,
                TRUE,
                $tbl
            );
            $collection = $collection->toArray();

            $tempCol = array_merge($tempCol,$collection['items']);
        }

        //get unique records
        $collection = array();
        foreach($tempCol as $key => $val){
            if(!in_array($val,$collection)){
                array_push($collection,$val);
            }
        }
        arsort($collection);
        return $collection;

    }

}
