<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'WwGallery.' . $_EXTKEY,
	'Pi1',
	array(
		'Gallery' => 'list, listFolder',
		
	),
	// non-cacheable actions
	array(
		'Gallery' => 'list, listFolder',
		
	)
);
