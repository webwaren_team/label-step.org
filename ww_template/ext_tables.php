<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

/***************
 * Default TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Template Webwaren');


$GLOBALS['TCA']['tt_content']['columns']['header']['config']['type'] = 'text';


$GLOBALS['TCA']['pages']['columns']['title']['config']['type'] = 'text';
$GLOBALS['TCA']['pages_language_overlay']['columns']['title']['config']['type'] = 'text';

/*

$temporaryColumn = array(

    'submenutitle' => array(
        'exclude' => 1,
        'label' => 'Submenu Titel',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'menusorting' => array(
        'exclude' => 1,
        'label' => 'Menu Sorting',
        'config' => array(
            'type' => 'input',
            'size' => 3,
            'eval' => 'trim'
        ),
    ),
    'disableinmobile' => array(
        'exclude' => 1,
        'label' => 'Disable in Mobile Version',
        'config' => array(
            'type' => 'check',
            'default' => '0'
        )
    ),

);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $temporaryColumn
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'submenutitle',
    'after:header'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'menusorting',
    'after:header'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'disableinmobile',
    'after:header'
);

*/



/***************
 * WebWaren BE Login
 */
if (TYPO3_MODE == 'BE') {

    $GLOBALS['TBE_STYLES']['logo'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Backend/Images/typo3logowebwaren.png';
    #$GLOBALS['TBE_STYLES']['logo_login'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Backend/Images/typo3logowebwaren.png';
}


$temporaryColumn2 = array(

    'submenutitle' => array(
        'exclude' => 1,
        'label' => 'Submenu Titel',
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'menusorting' => array(
        'exclude' => 1,
        'label' => 'Menu Sorting',
        'config' => array(
            'type' => 'input',
            'size' => 3,
            'eval' => 'trim'
        ),
    ),
    'disableinmobile' => array(
        'exclude' => 1,
        'label' => 'Disable in Mobile Version',
        'config' => array(
            'type' => 'check',
            'default' => '0'
        )
    ),

);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $temporaryColumn2
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'submenutitle',
    'after:header'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'menusorting',
    'after:header'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'general',
    'disableinmobile',
    'after:header'
);

