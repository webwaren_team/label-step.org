function hideLoadingLayer() {
	document.getElementById('hide_loading_site').style.display = 'none';
}

var myWidth = 0, myHeight = 0;
if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;

} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
}



(function($) {

	$(window).load(function(){

		// activate resize
		containerResize();
		$(window).bind('resize', function(){
			 containerResize();
		});

		$(window).bind('scroll', function(){
			containerScroll();
		});
		$('#hamburger').sidr({
			open: 'sidr-left',
			displace: false
		});

		$('#close').sidr({
			displace: false
		});

		$('#close').click(function() {
			$('.sidr').unbind('mouseleave');
		})


		// $(window).bind('scroll', function(){
		// 	containerScroll();
		// });

		$('.toTop').click(function () {
			$('html,body').animate({scrollTop:0});
		})

		/*SCROLL*/
		var scrollTop = $(window).scrollTop();
		var scrollToSubpart = $(location).attr('hash');

		if(scrollToSubpart){

			/*inital scrool to suppart*/
			$('html,body').animate({scrollTop:$(scrollToSubpart).offset().top -
			parseInt( $(scrollToSubpart).css( "border-top-width" ) ) -
			parseInt( $(scrollToSubpart).css( "margin-top" ) ) -
			parseInt( $(scrollToSubpart).css( "padding-top" ) )}, 0);
		}


		$("img.lazy").fadeIn();

		$('a[rel*="lightbox"]').magnificPopup({
                        type: 'image',
                        tLoading: 'Lade Bild...',
                        tClose: 'Schliessen (Esc)',
                        image: {
                                titleSrc: function(item) {
                                        var title = item.el.attr('title');
                                        var description = item.el.attr('alt');
                                        return ((title)?title:'') + ((description)?'<small>'+ description +'</small>':'');
                                }
                        },
                        gallery: {
                                enabled: true,
                                navigateByImgClick: true,
                                preload: [0,1],
                                tCounter: '%curr% von %total%',
                                tPrev: 'Zurück (Linke Pfeiltaste)',
                                tNext: 'Vorwärts (Rechte Pfeiltaste)'
                        },
		});

		// $('span.check').each(function () {
		// 	$(this).click(function () {
		// 		$(this).find('input').attr('checked','cheched');
		// 	})
		// })

		$('#listRetail').masonry();

		var $container = $('.galeryContainer');
		$container.imagesLoaded(function() {
			$container.masonry({
				// fitWidth: true,
				gutter: 5,
				itemSelector: '.galeryImage'
			});
		});

		$container.infinitescroll({
			navSelector  : ".f3-widget-paginator",
			nextSelector : ".f3-widget-paginator .next a",
			itemSelector : ".dataItem",
			appendCallback: false,
			debug        : false,

            // loading: {
			// 	img: "typo3conf/ext/ww_template/Resources/Public/Images/loading-spin.svg",
			// }

		}, function(newElements) {

			var $newElems = $( newElements ).css({ opacity: 0 });

			$container.append(newElements);

			$('.galeryContainer').masonry('layout');

			$newElems.imagesLoaded(function(){
				// show elems now they're ready
				$newElems.animate({ opacity: 1 });
				$container.masonry( 'appended', $newElems, true );
			});

		});


		hideLoadingLayer();

	});

	containerScroll = function () {
		var scrollTop = $(window).scrollTop();

		if(scrollTop >= 5) {
			// $('#interlude').css({'top':'-400px'});
			$('#interlude').css('transform','translate(0,-100%)');
		}else if(scrollTop <= 5) {
			$('#interlude').css('transform','translate(0,0)');
		}
	}
	containerResize = function () {

		if( typeof( window.innerWidth ) == 'number' ) {
		  //Non-IE
		  myWidth = window.innerWidth;
		  myHeight = window.innerHeight;
		} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		  //IE 6+ in 'standards compliant mode'
		  myWidth = document.documentElement.clientWidth;
		  myHeight = document.documentElement.clientHeight;

		} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		  //IE 4 compatible
		  myWidth = document.body.clientWidth;
		  myHeight = document.body.clientHeight;
		}

		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		var isiPhone = navigator.userAgent.match(/iPhone/i) != null;

		if(isiPad) {
		//	$('body').css("width","840");
		} else if (isiPhone){
		//	$('body').css("width","840");
		}

		if ($(window).width() > 767) {
			$('#geochart-colors').css('height',myHeight-60);
			$('#map,.gm-style,#map > div').css('height',myHeight-240);
		}

		var map = $('.haendler #map');

		$(map).each(function () {

			$(map).css('width',$(this).parent('div').width());
		 	$(map).css('height',$(this).parent('div').width()/4*3);
			$(map).css('min-width',$(this).parent('div').width());
			$(map).css('min-height',$(this).parent('div').width()/4*3);
			$(map).css('margin-left',26);
			$(map).css('margin-bottom',25);

			$('.gm-style,#map > div').css('height',$(this).parent('div').width()/4*3);


		})


		/*responsive images*/
		/*check browsersupport for mediaqueryies*/
		if(Modernizr.svg) {

			if (myWidth == 320) {
				$('img.lazy').each(function(index) {
					var imgsrc = $(this).data('small');
					if(imgsrc){
						$(this).attr('src',imgsrc);
					}
				});
			} else if (myWidth <= 720) {
				$('img.lazy').each(function(index) {
					var imgsrc = $(this).data('medium');
					if(imgsrc){
						$(this).attr('src',imgsrc);
					}
				});



			} else if (myWidth <= 940) {
				$('img.lazy').each(function(index) {
				    var imgsrc = $(this).data('large');
				    if(imgsrc){
					$(this).attr('src',imgsrc);
				    }
				});
			} else {

				$('img.lazy').each(function(index) {
					var imgsrc = $(this).data('bigger');
					if(imgsrc){
						$(this).attr('src',imgsrc);
					}
				});

			}


		}

		// $('.galeryContainer').masonry('layout');

	}

	//select country, retailgroup or retailtype
	$('.filterCountry, .filterGroup, input.retail').change(function(){
		$('input.retail').not(this).prop('checked', false);
		bounds = new google.maps.LatLngBounds();

		var count = $('input.retail:checkbox:checked').length;
		var cid = $('.filterCountry').val();
		var gid = $('.filterGroup').val();
		var retail = $('input.retail:checkbox:checked').val();

		var form = decodeURI($('#filterForm').attr('action'))+'&type=123';
		$.post(form,{data:1,country:cid,group:gid,retailType:retail},function (data){

			deleteMarkers();
			for (i = 0; i < data.length; i++){
				if(count == 1){
					if(retail == data[i]['retailType']){
						addMarker(data[i]['latitude'],data[i]['longitude'],data[i]['images'],data[i]['info_window'],data[i]['retailType']);
					}
				}else{
					addMarker(data[i]['latitude'],data[i]['longitude'],data[i]['images'],data[i]['info_window'],data[i]['retailType']);
				}
			}
			fillMarker(data,count,retail);

		});
	});

	// for searching
	$('input[name="searchKey"]').click(function(){
		bounds = new google.maps.LatLngBounds();
		var search = $('.searchKey').val();

		var form = decodeURI($('#filterForm').attr('action'))+'&type=123';
		$.post(form,{data:1,searchKey:search},function (data){

			deleteMarkers();
			for (i = 0; i < data.length; i++){
				addMarker(data[i]['latitude'],data[i]['longitude'],data[i]['images'],data[i]['info_window'],data[i]['retailType']);
			}
			fillMarker(data);
		});
	});

    $('#searchBoxLayer #closesearch').click(function () {
        $('#searchBoxLayer').fadeOut(150);
    })

    $('#searchBoxIcon').click(function () {
        $('#searchBoxLayer').fadeIn(150);
        $('#sword').focus();
        return false;
    })


    $("#searchbox form").on("submit",function(event) {

        var formaction = $(this).attr("action");

        var inputdata = $(this).serialize();
        var scrollTop = $(window).scrollTop();
        event.preventDefault();

        $("#searchResults")
            .text("")

            .addClass("loading");



        $.post(formaction+"&type=2", inputdata,
            function( data ) {



                $("#searchResults")

                    .css("display", "none")
                    .empty().append( data )
                    .removeClass("loading")
                    .fadeIn();

            }
        );


        return false;
    });

    $('ul.retailermainpage li a').attr("href", window.location.href + "#retailersList")


    function fillMarker(data) {



        $('#listRetail').html('');
        $.each(data, function (i, item) {
            html = "" +
                "<div class='col-md-4'><div class='title'><h2><a href='index.php?id=" + data[i]['uid'] + "&test="+ data[i]['language'] +"'>" +
                data[i]['title'] +
                // data[i]['title']+"<div class='image'>"+data[i]['images']+"</div>" +
                "</a></h2></div>" +

                "<div class='infowindow'>" + data[i]['info_window'] + "</div>" +

                "</div>";
            $('#listRetail').append(html);


        });
    }

	$(function() {
		// resize map
		$(window).on('load resize', function() {
			if (typeof google === 'object' && typeof google.maps === 'object') google.maps.event.trigger(map, "resize");
		});

		// convert img to background image
		$(".image", ".news-list2-container").each(function() {
			var $img = $("img", this);
			$img.parent().append('<div class="resized-img" />').find(".resized-img").css("background","url("+ $img.attr('src') +") no-repeat top center / cover");
		});

		$(".news-single-img", ".news-single-item").each(function() {
			var $img = $("img", this);
			$img.parent().append('<div class="resized-img" />').find(".resized-img").css("background","url("+ $img.attr('src') +") no-repeat top center / cover");
		});
	});
}(jQuery));
