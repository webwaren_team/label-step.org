### BaseURL
config.domain = www.label-step.org

[globalVar = IENV:HTTP_HOST = step.webwaren]
    config.domain = step.webwaren
[globalVar = IENV:HTTP_HOST = labelstep.wwaren.ch]
    config.domain = labelstep.wwaren.ch
[globalVar = IENV:HTTP_HOST = label-step:8888]
    config.domain = label-step:8888
[globalVar = IENV:HTTP_HOST = label-step.10.0.1.195.xip.io:8888]
    config.domain = label-step.10.0.1.195.xip.io:8888
[globalVar = IENV:HTTP_HOST = step.webw.ch]
    config.domain = step.webw.ch
[global]


config {
    adminPanel = 1
    debug = 1
}


PID {
    metanav = 6
    footerContent = 7
    404 = 4
    search = 4
    home = 1
}

## images width in columns
VAR {
    ## Image Width
    maxwidth = 2048
    2colwidth = 585
    3colwidth = 390
    4colwidth = 295
    leftmaxwidth = 70
    rimaxwidth = 70

    ## Show / Hide
    showMainNav = 1
    showTray1 = 0
    showMetaNav = 1
    showSearchBox = 1
    showLangNav = 1
    showFooter = 1
    showCopyright = 1
    showFooterNav = 1
    showSvnVersion = 0

    logo = EXT:ww_template/Resources/Public/Images/logo.png
    logo.width = 430
    logo.height = 96

}

## fluid styled content
styles.templates {
    # cat=content/templates/b1; type=string; label= Path of Fluid Templates for all defined content elements
    templateRootPath = typo3conf/ext/ww_template/Resources/Private/Templates/Content/
    # cat=content/templates/b2; type=string; label= Path of Fluid Partials for all defined content elements
    partialRootPath = typo3conf/ext/ww_template/Resources/Private/Partials/Content/
    # cat=content/templates/b3; type=string; label= Path of Fluid Layouts for all defined content elements
    layoutRootPath = typo3conf/ext/ww_template/Resources/Private/Layouts/Content/

}

PATH {

    fluidtemplate.templates = typo3conf/ext/ww_template/Resources/Private/Templates/Page/
    fluidtemplate.layoutRootPath = typo3conf/ext/ww_template/Resources/Private/Layouts/Page/
    fluidtemplate.partialRootPath = typo3conf/ext/ww_template/Resources/Private/Partials/
    images = typo3conf/ext/ww_template/Resources/Public/Images/
    css = typo3conf/ext/ww_template/Resources/Public/Stylesheets/
    extcss = typo3conf/ext/ww_template/Resources/Public/Stylesheets/ext/
    js = typo3conf/ext/ww_template/Resources/Public/Javascript/
    html = typo3conf/ext/ww_template/Resources/Public/Html/
}

FILE {
    default_labels = EXT:ww_template/Resources/Private/Language/locallang.xlf
}

TEXT {
    lang = de
    language1 = DE
	language2 = FR
	language3 = EN
	language4 = IT
}

PROJECT {
    jswindowsize = 800x600
    jswindowparams = status=1,menubar=0,scrollbars=1,resizable=1,location=0,directories=0,toolbar=0
}

## css_styled_content
styles.content.loginform.pid = 75
styles.content.imgtext.maxWInText = 0
styles.content.imgtext.layoutKey = data
styles.content.imgtext.linkWrap.width = 800



[globalVar = GP:L = 1]
    page.meta.language.value = français, fr
    TEXT.lang = fr
    TEXT.langUid = 1
[global]

[globalVar = GP:L = 2]
    page.meta.language.value = italien, it
    TEXT.lang = it
    TEXT.langUid = 2
[global]

[globalVar = GP:L = 3]
    page.meta.language.value = english, en
    TEXT.lang = en
    TEXT.langUid = 3
[global]



plugin.tx_femanager.view {
	    templateRootPath = EXT:ww_template/Resources/Private/Templates/Femanager/
		partialRootPath = EXT:ww_template/Resources/Private/Partials/Femanager/
		layoutRootPath = EXT:ww_template/Resources/Private/Layouts/Femanager/
}
