page.meta {

	description.data = LLL:{$FILE.default_labels}:meta.description
	description.override.field = description

	author.data = LLL:{$FILE.default_labels}:meta.author
	author.override.field = author

	keywords.data = LLL:{$FILE.default_labels}:meta.keywords
	keywords.override.field = keywords

	language.value = deutsch, de

	robots = index,follow

}
