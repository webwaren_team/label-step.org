lib.menuDefault = HMENU
lib.menuDefault {

	1 = TMENU
	1 {
		showAccessRestrictedPages = {$PID.401}

		showAccessRestrictedPages.insertData=1
		showAccessRestrictedPages.addParams = &redirect_url=###RETURN_URL###&pageId=###PAGE_ID###

		wrap = <ul>|</ul>
		expAll = 1

		noBlur = 1

		NO = 1
		NO {

			doNotLinkIt = 1
			stdWrap.cObject = CASE
			stdWrap.cObject {
			  key.field = doktype
			  default = TEXT
			  default {
			    field = nav_title // title
			    typolink.parameter.field = uid
				  typolink.ATagParams = class="level1"
			    stdWrap.htmlSpecialChars = 0
			  }

			  # 3 = external url
			  3 = TEXT
			  3 {
			    field = nav_title // title
			    typolink.parameter.field = url
			    typolink.extTarget = _blank
			  }

			}


			ATagTitle {
				field = title // nav_title
				fieldRequired = title
			}


               		wrapItemAndSub = <li class="first">|</li> |*| <li>|</li> |*| <li class="last">|</li>

               		stdWrap.htmlSpecialChars = 0


		}

		ACT < .NO
		CUR < .NO

		CUR.wrapItemAndSub = <li class="first cur">|</li>||<li class="second cur">|</li> |*| <li class="cur">|</li> |*| <li class="cur last">|</li>
		ACT.wrapItemAndSub = <li class="first act">|</li>||<li class="second act">|</li> |*| <li class="act">|</li> |*| <li class="last act">|</li>

		IFSUB = 1
		IFSUB < .CUR
		IFSUB.wrapItemAndSub.insertData=1
		IFSUB.wrapItemAndSub = <li class="first hasSub pid{field:uid}">|</li>||<li class="second hasSub pid{field:uid}">|</li> |*| <li class="hasSub pid{field:uid}">|</li> |*| <li class="hasSub last pid{field:uid}">|</li>

	}
}

[usergroup = *]
	lib.menuDefault.1.NO.ATagParams >
[global]

lib.menuDefault.1 {

	ACT {
		ATagParams >
		ATagParams = class="act"
	}


	CUR {
		ATagParams >
		ATagParams = class="cur"
	}
}
